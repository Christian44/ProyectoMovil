
import {Injectable} from '@angular/core';
import { Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';



@Injectable()
export class usuarioService {

  constructor(public http: Http) {
  	console.log('Hello usuarioService Provider');
  }

  	getAll()
  	{
  		return new Promise(
  				resolve=>{
  					this.http.get("http://127.0.0.1:8000/api/usuario/")
  					.map(res=>res.json())
  					.subscribe(
  							data =>{
  								resolve(data);
  							},
  							err=>{
  								console.log(err);
  							}
  						)
  				}
  			);

  	}


	getOne(id)
	  	{
	  		return new Promise(
	  				resolve=>{
	  					this.http.get("http://127.0.0.1:8000/api/usuario/one/"+id)
	  					.map(res=>res.json())
	  					.subscribe(
	  							data =>{
	  								resolve(data);
	  							},
	  							err=>{
	  								console.log(err);
	  							}
	  						)
	  				}
	  			);

	  	}




	 add(data)
  	{
  		return new Promise(
  				resolve=>{
  					this.http.post("http://127.0.0.1:8000/api/usuario/data/",data)
  					.map(res=>res.json())
  					.subscribe(
  							data =>{
  								resolve(data);
  							},
  							err=>{
  								console.log(err);
  							}
  						)
  				}
  			);

  	}


  	 addVeces(data)
  	{
  		return new Promise(
  				resolve=>{
  					this.http.put("http://127.0.0.1:8000/api/usuario/updateVeces",data)
  					.map(res=>res.json())
  					.subscribe(
  							data =>{
  								resolve(data);
  							},
  							err=>{
  								console.log(err);
  							}
  						)
  				}
  			);

  	}


  	 update(data)
  	{
  		return new Promise(
  				resolve=>{
  					this.http.put("http://127.0.0.1:8000/api/usuario/",data)
  					.map(res=>res.json())
  					.subscribe(
  							data =>{
  								resolve(data);
  							},
  							err=>{
  								console.log(err);
  							}
  						)
  				}
  			);

  	}


  	 delete(id)
  	{
  		return new Promise(
  				resolve=>{
  					this.http.delete("http://127.0.0.1:8000/api/usuario/delete/"+id)
  					.map(res=>res.json())
  					.subscribe(
  							data =>{
  								resolve(data);
  							},
  							err=>{
  								console.log(err);
  							}
  						)
  				}
  			);

  	}


}




/*
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

}
*/