import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ModalPage } from './modal-page';

import { usuarioEditar} from '../usuarioEditar/usuarioEditar';
import {usuarioService} from '../../providers/usuarioService';
import { contactos } from '../contactos/contactos';
import { practicas } from '../practicas/practicas';



@Component({
  selector: 'usuarioEditarInfo',
  templateUrl: 'usuarioEditarInfo.html',
  providers: [usuarioService]
})

export class usuarioEditarInfo {
	info;
	name;
	email;

	posts: any;
  private searchQuery: string = '';
  private items: string[];
  public codigo: Int16Array;


  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http, public servicios: usuarioService, public alertCtrl: AlertController) {
  
  	this.info=this.navParams.get('value')
    this.codigo=this.info

	this.initializeItems();

    this.http.get('http://127.0.0.1:8000/api/usuario/')
    .map(res => res.json()).subscribe(data => {
        this.posts = data;
        console.log(this.posts);
    });







  }

initializeItems() {
    this.items = this.posts;

  }











  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {

      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })

    }
  }


enviar(){
	var datos={};
	datos["name"]= this.name;
	datos["email"] = this.email;
	datos["id"]= this.codigo;
	this.servicios.update(datos)
	.then(
			data =>{
				console.log(data);
				if(data["response"]=="ok"){
					let alert = this.alertCtrl.create(
							{
								title:"Aviso",
								subTitle:"Registro Modificado",
								buttons:["Ok"]
							}
						);
					alert.present();
				}
			}
		)

}

/*







actualizarUsuario()
{
	var usuarioDatos= this.posts;
	usuarioDatos.id=this.codigo;
    this.http.put('http://127.0.0.1:8000/api/usuario/'+usuarioDatos)
    			.success(function (){ console.log("exitoso")});
    /*
	var requestoptions = new RequestOptions({
            method: RequestMethod.Put,
            url: 'http://127.0.0.1:8000/api/usuario/' ,
            headers:  new Headers({
 		   'Content-Type': 'application/json'
 			 }),
            body: JSON.stringify(usuarioDatos)
        });
}
*/

onGuardar(){

this.navCtrl.push(usuarioEditar);

}


onEditar(post)
{
this.navCtrl.push(usuarioEditarInfo, {
   'value':post
  })
}

onCancelar(){


}

onClickC()
  {
  this.navCtrl.push(contactos)
  }

  onClickP()
  {
  this.navCtrl.push(practicas)
  }


}
