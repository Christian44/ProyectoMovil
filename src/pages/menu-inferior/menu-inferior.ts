import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { Noticias} from '../noticias/noticias';
import { Publicaciones} from '../publicaciones/publicaciones';
import { Proyectos } from '../proyectos/proyectos';
import { Usuarios } from '../usuarios/usuarios';

@Component({
  templateUrl: 'menu-inferior.html'
})

export class MenuInferior {

  tab1Root: any = Noticias;
  tab2Root: any = Publicaciones;
  tab3Root: any = Proyectos;
  tab4Root: any = Usuarios;

  constructor() {}

}
