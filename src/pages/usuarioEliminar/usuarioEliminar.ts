import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { contactos } from '../contactos/contactos';
import { practicas } from '../practicas/practicas';

@Component({
  selector: 'usuarioEliminar',
  templateUrl: 'usuarioEliminar.html'
})
export class usuarioEliminar {

  constructor(public navCtrl: NavController) {

  }



  onClickC()
  {
  this.navCtrl.push(contactos)
  }

  onClickP()
  {
  this.navCtrl.push(practicas)
  }

}
