import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { contactos } from '../contactos/contactos';
import { practicas } from '../practicas/practicas';

@Component({
  selector: 'page-noticias',
  templateUrl: 'noticias.html',
})
export class Noticias {

  constructor(public navCtrl: NavController) {

  }


   onClickC()
  {
  this.navCtrl.push(contactos)
  }

  onClickP()
  {
  this.navCtrl.push(practicas)
  }

}