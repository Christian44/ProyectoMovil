import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { ModalController } from 'ionic-angular';
import { ModalPage } from './modal-page';
import { usuarioEliminar} from '../usuarioEliminar/usuarioEliminar';
import { usuarioEditarInfo} from '../usuarioEditarInfo/usuarioEditarInfo';
import { contactos } from '../contactos/contactos';
import { practicas } from '../practicas/practicas';



import { Usuarios } from '../usuarios/usuarios';

@Component({
  selector: 'usuarioEditar',
  templateUrl: 'usuarioEditar.html'
})


export class usuarioEditar {
info;


 posts: any;
  private searchQuery: string = '';
  private items: string[];
  public codigo: Int16Array;

constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http)
{
 this.info=this.navParams.get('value')
 this.codigo=this.info
 
 this.initializeItems();

    this.http.get('http://127.0.0.1:8000/api/usuario/')
    .map(res => res.json()).subscribe(data => {
        this.posts = data;
        console.log(this.posts);
    });

}


initializeItems() {
    this.items = this.posts;

  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {

      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })

    }
  }


onEditar(post)
{
this.navCtrl.push(usuarioEditarInfo, {
   'value':post
  })
}


onEliminar(post)
{
this.navCtrl.push(usuarioEliminar, {
   'value':post
  })
}






onClickC()
  {
  this.navCtrl.push(contactos)
  }

  onClickP()
  {
  this.navCtrl.push(practicas)
  }

  }

 
