import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { libros } from '../libros/libros';
import { Http, Headers } from '@angular/http';
import { contactos } from '../contactos/contactos';
import { practicas } from '../practicas/practicas';



@Component({
  selector: 'page-publicaciones',
  templateUrl: 'publicaciones.html',
})
export class Publicaciones {

   public   data : any;
  constructor(public navCtrl: NavController, public http: Http) {
    
     let header = new Headers();
    header.append('Accept','application/json');
    header.append('Authorization','Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJmN2JiMWYxYmYyZmFiNjNlZDU4ZTM2MTZmYjBjM2Q1YjMwYzI5MTQ2MjgyYzRlMWQxYmI1YTQxYmZmMTJlZGQ4ZDJmYTI3MTAwNjM3ZGRjIn0.eyJhdWQiOiIxIiwianRpIjoiYmY3YmIxZjFiZjJmYWI2M2VkNThlMzYxNmZiMGMzZDViMzBjMjkxNDYyODJjNGUxZDFiYjVhNDFiZmYxMmVkZDhkMmZhMjcxMDA2MzdkZGMiLCJpYXQiOjE0OTI5MTk2ODQsIm5iZiI6MTQ5MjkxOTY4NCwiZXhwIjoxNTI0NDU1Njg0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.IEZ2WV3CLQYXITRri9fA7Ascda0QuZ4IXEp0VNvOfB8EfwA3y9qyupyN5JcegWqQ5nI8C-8steC846UgTYVipkVMiLn3NfAJsiiYC6iN_dQqAGUpG1bkPoNCZZME0sNB_gVBgj6d6FxpthAdVrDMvyjdvfgrdfDZ2Hh_hkZykzDShu0dxE0KLJrqmH7sINbZNvsNGJMHH3BPY6QMKoZpnt4LROZSfuzw5RbOT42M3SUoOVGVdbWBczp0BCQlnCMr3NIAwFP66kvmErKien5yGENLyN1DT7Iy9dWkIssw81bo8dNA6nBA7FpmyqFhFiJ8vvkwdw2bGOnwAb2CfZ6hGOfi0ZBft9JL1p42hbhT0Vo1G1UGcWco3fIXX55XpKRRHD3PL50-h-URmxZBm6jAu7bRoWRXVc1cEX9lvKB2SInUA1DLHsWrEzx1HHxW7t4QhfoXTsNsZWj_V-t63kbu5-IjokTslxBTJwSO7IFPVWX1vWPw1hGi8P6BJweTs7BPQxAo8FSZL_TKZhIuRKt8OAWqzHnnvCVpWzGeugnlwIdfuM5RBwlxhBko5BCxQh7RnHJABjjY2NlwNRanhRUfZRcpDEKSszk4Ua1qJzfV1CJEHk9nxjh8C9UH1nR5EyKsy4ZvciURJH-o3jXAVpngz-g-pw_oLQi5s9aaunsxGXM ')

    this.http.get('http://localhost:8000/api/usuario',{headers: header})
      .map(res => res.json())
      .subscribe(
      data => {
        this.data = data;
      },
      err => {
        console.log('error')
      })
}






onClickC()
  {
  this.navCtrl.push(contactos)
  }

  onClickP()
  {
  this.navCtrl.push(practicas)
  }
}

